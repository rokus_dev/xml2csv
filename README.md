# Xml2Csv

Executable file (located in resources) is generated using Launch4J

The configuration for the Launch4J used can also be found in the resources folder under 4JConfig.xml

If you want to use Launch4J to create an executable locally, you will need to update the jar, outfile, icon, and splash file locations in the provided 4JConfig.xml to map correctly to your own local directory for these resources.

These resources are all included within this repo.

The icon used for the executable shortcut, and splashscreen image are included in the resources folder.

The jar is located in the out/artifacts/Xml2Csv_jar folder

The outfile is wherever you want the executable to be created, by Launch4J