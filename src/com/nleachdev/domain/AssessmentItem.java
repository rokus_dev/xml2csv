package com.nleachdev.domain;

import java.util.Objects;
import java.util.StringJoiner;

public class AssessmentItem {
    private String identifier;
    private String title;
    private String label;
    private String correctChoice;
    private Prompt prompt = new Prompt();

    public AssessmentItem() {

    }

    public AssessmentItem(final String identifier, final String title, final String label, final String correctChoice, final Prompt prompt) {
        this.identifier = identifier;
        this.title = title;
        this.label = label;
        this.correctChoice = correctChoice;
        this.prompt = prompt;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(final String identifier) {
        this.identifier = identifier;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public String getCorrectChoice() {
        return correctChoice;
    }

    public void setCorrectChoice(final String correctChoice) {
        this.correctChoice = correctChoice;
    }

    public Prompt getPrompt() {
        return prompt;
    }

    public void setPrompt(final Prompt prompt) {
        this.prompt = prompt;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final AssessmentItem that = (AssessmentItem) o;
        return Objects.equals(identifier, that.identifier) &&
                Objects.equals(title, that.title) &&
                Objects.equals(label, that.label) &&
                Objects.equals(correctChoice, that.correctChoice) &&
                Objects.equals(prompt, that.prompt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identifier, title, label, correctChoice, prompt);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AssessmentItem.class.getSimpleName() + "[", "]")
                .add("identifier='" + identifier + "'")
                .add("title='" + title + "'")
                .add("label='" + label + "'")
                .add("choice='" + correctChoice + "'")
                .add("prompt=" + prompt)
                .toString();
    }
}

