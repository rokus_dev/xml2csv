package com.nleachdev.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Prompt {
    private String promptQuestion;
    private List<SimpleChoice> simpleChoices = new ArrayList<>();

    public Prompt() {

    }

    public Prompt(final String promptQuestion, final List<SimpleChoice> simpleChoices) {
        this.promptQuestion = promptQuestion;
        this.simpleChoices = simpleChoices;
    }

    public String getPromptQuestion() {
        return promptQuestion;
    }

    public void setPromptQuestion(final String promptQuestion) {
        this.promptQuestion = promptQuestion;
    }

    public List<SimpleChoice> getChoices() {
        return simpleChoices;
    }

    public void setChoices(final List<SimpleChoice> simpleChoices) {
        this.simpleChoices = simpleChoices;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Prompt prompt1 = (Prompt) o;
        return Objects.equals(promptQuestion, prompt1.promptQuestion) &&
                Objects.equals(simpleChoices, prompt1.simpleChoices);
    }

    @Override
    public int hashCode() {
        return Objects.hash(promptQuestion, simpleChoices);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Prompt.class.getSimpleName() + "[", "]")
                .add("prompt='" + promptQuestion + "'")
                .add("choices=" + simpleChoices)
                .toString();
    }
}
