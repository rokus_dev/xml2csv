package com.nleachdev.domain;

import java.util.Objects;
import java.util.StringJoiner;

public class SimpleChoice {
    private String simpleChoice;
    private String choiceIdentifier;

    public SimpleChoice() {

    }

    public SimpleChoice(final String simpleChoice, final String choiceIdentifier) {
        this.simpleChoice = simpleChoice;
        this.choiceIdentifier = choiceIdentifier;
    }

    public String getSimpleChoice() {
        return simpleChoice;
    }

    public void setSimpleChoice(final String simpleChoice) {
        this.simpleChoice = simpleChoice;
    }

    public String getChoiceIdentifier() {
        return choiceIdentifier;
    }

    public void setChoiceIdentifier(final String choiceIdentifier) {
        this.choiceIdentifier = choiceIdentifier;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SimpleChoice simpleChoice1 = (SimpleChoice) o;
        return Objects.equals(simpleChoice, simpleChoice1.simpleChoice) &&
                Objects.equals(choiceIdentifier, simpleChoice1.choiceIdentifier);
    }

    @Override
    public int hashCode() {
        return Objects.hash(simpleChoice, choiceIdentifier);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SimpleChoice.class.getSimpleName() + "[", "]")
                .add("choice='" + simpleChoice + "'")
                .add("identifier='" + choiceIdentifier + "'")
                .toString();
    }
}
