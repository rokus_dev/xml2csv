package com.nleachdev.io;

import com.nleachdev.domain.AssessmentItem;
import com.nleachdev.domain.SimpleChoice;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class XmlParser {
    private static final Logger logger = Logger.getLogger(XmlParser.class.getName());

    private XmlParser() {
        throw new UnsupportedOperationException("I am static utility class, you shouldn't be trying to instantiate me");
    }

    public static AssessmentItem readFile(final String path) {
        final AssessmentItem item = new AssessmentItem();
        try (final BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;

            while ((line = br.readLine()) != null) {
                /*
                    Grab relevant data from <assessmentItem> tag
                 */
                if (line.contains("<assessmentItem")) {
                    final String mainAttributes = getMainAttributes(line);
                    item.setIdentifier(getDelimited(mainAttributes, "identifier"));
                    item.setTitle(getDelimited(mainAttributes, "title"));
                    item.setLabel(getDelimited(mainAttributes, "label"));
                }

                /*
                    Grab relevant choice data
                 */
                if (line.contains("![CDATA[")) {
                    item.setCorrectChoice(getChoice(line));
                }

                /*
                    Grab prompt data
                 */
                if (line.contains("prompt")) {
                    item.getPrompt().setPromptQuestion(getValue(line));
                }

                /*
                    Grab the multiple choice data from <simpleChoice> tag
                 */
                if (line.contains("simpleChoice")) {
                    final SimpleChoice simpleChoice = new SimpleChoice(
                            getValue(line), getDelimited(line, "identifier")
                    );
                    item.getPrompt().getChoices().add(simpleChoice);
                }
            }

            return item;
        } catch (final Exception e) {
            logger.log(Level.SEVERE, "Exception thrown when parsing XML file", e);
            return item;
        }
    }

    /*
        The choice is part of the <value> tag

        An ex is as follows: <value><![CDATA[choice_3]]></value>

        We want the portion within the CDATA construct, in this ex "choice_3"
     */
    private static String getChoice(final String line) {
        int cur = line.indexOf("![CDATA[") + 8;
        final StringBuilder sb = new StringBuilder();
        while (true) {
            final char c = line.charAt(cur);
            if (String.valueOf(c).equals("]")) {
                break;
            }
            sb.append(c);
            cur++;
        }

        return sb.toString();
    }

    /*
        The main attributes are attributes of the <assessmentItem> tag;

        An ex is as follows: identifier="i1597526996080118900904" title="Grade 11_Astronomy_C1A-1" label="Grade 11_Astronomy_C1A-1"

        Grab the starting index of the identifier value and the end index of the label value, for the current tag/line
     */
    private static String getMainAttributes(final String line) {
        final int start = line.indexOf("identifier");
        final int labelStart = line.indexOf("label");
        int end = start;
        int cur = labelStart + 7;
        while (true) {
            final char c = line.charAt(cur);
            if (String.valueOf(c).equals("\"")) {
                end = cur;
                break;
            }
            cur++;
        }

        return line.substring(start, end);
    }

    /*
        Get data from corresponding attribute string

        Ex: <simpleChoice identifier="choice_1" fixed="false" showHide="show">10</simpleChoice>

        If we want the identifier value, in this case "choice_1", we pass the line, and the attribute we want (in this case, identifier)
     */
    private static String getDelimited(final String str, final String attribute) {
        int cur = str.indexOf(attribute) + attribute.length() + 2;
        final StringBuilder sb = new StringBuilder();
        while (true) {
            if (str.length() <= cur) {
                break;
            }
            final char c = str.charAt(cur);
            if (String.valueOf(c).equals("\"")) {
                break;
            }
            sb.append(c);
            cur++;
        }

        return sb.toString();
    }

    /*
        Get value of xml tag on current line;

        For ex:

        <SomeTag someAttribute="asdf">123</SomeTag>

        In that ^, the value is 123, not "asdf" (which is the attribute)
     */
    private static String getValue(final String line) {
        int first = line.indexOf(">") + 1;
        int last = line.lastIndexOf("</");

        final String value = line.substring(first, last);
        return value.replaceAll("&nbsp", " ");
    }

    private static String replaceSmartQuotes(final String line) {
        return line.replaceAll("[\\u2018\\u2019]", "'")
                .replaceAll("[\\u201C\\u201D]", "\"");
    }
}
