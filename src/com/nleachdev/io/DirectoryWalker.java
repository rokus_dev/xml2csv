package com.nleachdev.io;

import com.nleachdev.domain.AssessmentItem;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.file.FileVisitResult.CONTINUE;

public class DirectoryWalker extends SimpleFileVisitor<Path> {
    private static final Logger logger = Logger.getLogger(DirectoryWalker.class.getName());
    private final Path rootDirectory;
    private int fileCount = 0;

    public DirectoryWalker(final Path rootDirectory) {
        this.rootDirectory = rootDirectory;
    }

    @Override
    public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) {
        logger.log(Level.INFO, "XML being parsed is approximately " + attrs.size() + "bytes");

        if (file.getFileName().endsWith("qti.xml")) {
            parseAndConvert(file.toFile());
            fileCount++;
        }

        return CONTINUE;
    }

    private void parseAndConvert(final File file) {
        final AssessmentItem item = XmlParser.readFile(file.getAbsolutePath());
        try {
            CsvWriter.writeItemToFile(item, rootDirectory);
        } catch (final IOException e) {
            logger.log(Level.SEVERE, "Exception thrown when writing item to file", e);
        }
    }

    public int getFileCount() {
        return fileCount;
    }
}