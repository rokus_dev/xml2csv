package com.nleachdev.io;

import com.nleachdev.domain.AssessmentItem;
import com.nleachdev.domain.SimpleChoice;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.StringJoiner;
import java.util.function.BiFunction;

public final class CsvWriter {
    private static final String HEADER_ROW = "identifier\ttitle\tlabel\tns1:value\tns1:prompt\tns1:simpleChoice\tidentifier10";
    private static final BiFunction<AssessmentItem, SimpleChoice, String> lineGenerator = (item, simpleChoice) ->
            new StringJoiner("\t")
                    .add(item.getIdentifier())
                    .add(item.getTitle())
                    .add(item.getLabel())
                    .add(item.getCorrectChoice())
                    .add(item.getPrompt().getPromptQuestion())
                    .add(simpleChoice.getSimpleChoice())
                    .add(simpleChoice.getChoiceIdentifier()).toString();

    private CsvWriter() {
        throw new UnsupportedOperationException("I am static utility class, you shouldn't be trying to instantiate me");
    }

    public static void writeItemToFile(final AssessmentItem item, final Path rootDirectory) throws IOException {
        final File file = new File(rootDirectory.toAbsolutePath().toString() + "\\out.csv");
        final boolean exists = file.exists();
        try (final PrintWriter writer = new PrintWriter(new FileWriter(file, true))) {
            if (!exists) {
                writer.println(HEADER_ROW);
            }

            item.getPrompt()
                    .getChoices()
                    .forEach(simpleChoice ->
                            writer.println(lineGenerator.apply(item, simpleChoice))
                    );
        }
    }

}
