package com.nleachdev;

import com.nleachdev.io.DirectoryWalker;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainApplication {
    private static final Logger logger = Logger.getLogger(MainApplication.class.getName());

    public static void main(final String[] args) throws InterruptedException {
        Thread.sleep(2500); // give time for the splash screen to show :) (splash creen made with launch4j) will only be seen if you run executable
        javax.swing.SwingUtilities.invokeLater(MainApplication::createAndShowGUI);
    }


    private static void createAndShowGUI() {
        final JFrame frame = getFrame();

        final JButton button = createButton(null);
        button.addActionListener(event ->
                handleFileExplorer(frame)
        );
        frame.getContentPane().add(button, BorderLayout.CENTER);

        showFrame(frame);
    }

    private static void handleFileExplorer(final JFrame frame) {
        int fileCount = 0;
        frame.setVisible(false);

        final JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        final int option = fileChooser.showOpenDialog(frame);
        if (option == JFileChooser.APPROVE_OPTION) {
            final File file = fileChooser.getSelectedFile();
            fileCount = walkDirectoryPath(file.toPath());

            clearFrame(frame);
            addCompletionWindow(frame, fileCount);
            showFrame(frame);
        }
    }

    private static int walkDirectoryPath(final Path rootDirectory) {
        try {
            final DirectoryWalker fileVisitor = new DirectoryWalker(rootDirectory);
            Files.walkFileTree(rootDirectory, fileVisitor);
            return fileVisitor.getFileCount();
        } catch (final IOException e) {
            logger.log(Level.SEVERE, "Exception thrown when parsing file tree", e);
            return 0;
        }
    }

    private static JFrame getFrame() {
        final JFrame frame = new JFrame("Shogun's Xml2Csv");
        frame.setMinimumSize(new Dimension(1000, 240));

        final JPanel contentPanel = new JPanel();
        final Border padding = BorderFactory.createEmptyBorder(10, 10, 10, 10);
        contentPanel.setBorder(padding);
        frame.setContentPane(contentPanel);

        final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));


        frame.getContentPane().add(createLabel("Magically Convert Your XML Data To CSV!!!"));

        return frame;
    }

    private static void addCompletionWindow(final JFrame frame, final int fileCount) {
        frame.getContentPane().add(createLabel("Finished converting " + fileCount + " files!!!"));
        final JButton button = createButton("Click To Exit");
        button.addActionListener(event ->
                System.exit(0)
        );
        frame.getContentPane().add(button, BorderLayout.CENTER);
    }

    private static JLabel createLabel(final String labelText) {
        JLabel label = new JLabel(labelText);
        centerAndAddPadding(label, 10);
        return label;
    }

    private static JButton createButton(final String buttonText) {
        final JButton button = new JButton();
        if (buttonText == null) {
            button.setIcon(createImageIcon());
        } else {
            button.setText(buttonText);
        }
        centerAndAddPadding(button, 15);
        return button;
    }

    private static void showFrame(final JFrame frame) {
        frame.pack();
        frame.setVisible(true);
    }

    private static void centerAndAddPadding(final JComponent element, final int borderVal) {
        element.setBorder(BorderFactory.createEmptyBorder(borderVal, borderVal, borderVal, borderVal));
        element.setAlignmentX(Component.CENTER_ALIGNMENT);
    }

    private static void clearFrame(final JFrame frame) {
        frame.getContentPane().removeAll();
    }

    protected static ImageIcon createImageIcon() {
        URL imgURL = MainApplication.class.getClassLoader().getResource("com/nleachdev/resources/converterIcon.png");
        return new ImageIcon(imgURL);
    }
}
